from __future__ import unicode_literals 
import uuid 
from django.db import models
from django.utils.formats import get_format

from django.contrib.gis.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

from starbuckshack.helpers import PathAndRename

from users.models import (

    CustomUser
)


class Asset(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    name = models.CharField(max_length=20, default='NA')
    coordinates = models.PointField()
    area = models.PolygonField()

    ASSET_TYPE = [
        ('PL', 'Plant'),
        ('EL', 'Electrical'),
        ('NA', 'Not Available'),   
    ]

    asset_type = models.CharField(
        max_length=2,
        choices=ASSET_TYPE,
        default='NA',
    ) 

    STATUS = [
        ('USE', 'Asset is in Use'),
        ('BER', 'Beyond Economic Repair'),
        ('NA', 'Not Available'),   
    ]

    status = models.CharField(
        max_length=3,
        choices=STATUS,
        default='NA',
    ) 

    asset_status = models.BooleanField(blank=False, default=True)

    

    def __str__(self):
        return self.name

    
    


    


