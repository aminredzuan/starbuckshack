# users/models.py
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
import uuid

from django.contrib.auth.models import AbstractUser
from django.db import models



from starbuckshack.helpers import PathAndRename

class CustomUser(AbstractUser):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(blank=True, max_length=255)
    profile_picture = models.ImageField(null=True, upload_to=PathAndRename('images'))

    USER_TYPE = [
        ('EC', 'End Client'),
        ('AD', 'Administrator'),
        ('EV', 'Evaluator'),
        ('NA', 'Not Available'),   
    ]

    user_type = models.CharField(
        max_length=2,
        choices=USER_TYPE,
        default='NA',
    )     
    

    address = models.CharField(max_length=900, null=True, default='NA')
    telephone_number = models.CharField(max_length=20, default='NA') 
    mobile_number = models.CharField(max_length=20, default='NA') 
    ic_number = models.CharField(blank=False,max_length=20, default='NA')
    occupation = models.CharField(max_length=60, null=True, default='NA')    
    
    def __str__(self):
        return self.name